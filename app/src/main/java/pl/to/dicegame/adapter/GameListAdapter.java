package pl.to.dicegame.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.to.dicegame.R;
import pl.to.dicegame.model.User;
import pl.to.dicegame.model.game.AbstractGame;
import pl.to.dicegame.model.game.Game;

public class GameListAdapter extends BaseAdapter {

    private List<AbstractGame> games;
    private Context context;

    public GameListAdapter(Context context, List<AbstractGame> games){
        this.context = context;
        this.games = games;
    }

    public void updateList() {
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return games.size();
    }

    @Override
    public Object getItem(int position) {
        return games.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_game, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.playersNameText.setText(concatenetPlayers(games.get(position).getUser()));

        if(games.get(position).isStarted()){
            holder.joinButton.setText("Join as Observer");
            holder.stateText.setText("Game already started");
        } else {
            holder.joinButton.setText("Join");
            holder.stateText.setText("In Lobby");
        }

        return convertView;
    }

    private String concatenetPlayers(List<User> users){
        String players = "";
        for(User user : users){
            players = user.getName() + ", " + players;
        }
        return players;
    }

    class ViewHolder {
        @BindView(R.id.players) TextView playersNameText;
        @BindView(R.id.state_of_game) TextView stateText;
        @BindView(R.id.join) Button joinButton;

        ViewHolder(View view){
            ButterKnife.bind(this, view);
        }
    }
}
