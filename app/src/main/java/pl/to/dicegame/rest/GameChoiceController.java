package pl.to.dicegame.rest;

import java.util.List;

import pl.to.dicegame.model.game.Game;

/**
 * Created by hector on 14.12.16.
 */

public interface GameChoiceController {

    boolean joinAsObserver(String nick, String gameId);
    boolean joinAsPlayer(String nick, String gameId);
    List<Game> getAvailableGames();
}
