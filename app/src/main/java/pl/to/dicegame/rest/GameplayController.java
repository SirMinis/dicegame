package pl.to.dicegame.rest;

import pl.to.dicegame.model.move.Move;

/**
 * Created by hector on 14.12.16.
 */

public interface GameplayController {

    void quitGame(String gameId);
    boolean makeMove(Move move);
}
