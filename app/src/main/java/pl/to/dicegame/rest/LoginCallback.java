package pl.to.dicegame.rest;

/**
 * Created by Dominik Jura on 2016-12-13.
 */

public interface LoginCallback {
    void onSuccess();
    void onFailure();
}
