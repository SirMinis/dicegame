package pl.to.dicegame.rest;

import java.util.List;

import pl.to.dicegame.model.Configuration;
import pl.to.dicegame.model.User;
import pl.to.dicegame.model.game.Game;

/**
 * Created by hector on 14.12.16.
 */

public interface ConfigurationRoomController {

    Game createNewGame(Configuration configuration);
    List<User> getJoinedPlayers(String gameId);
}
