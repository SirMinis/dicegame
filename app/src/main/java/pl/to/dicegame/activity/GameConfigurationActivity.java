package pl.to.dicegame.activity;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.aigestudio.wheelpicker.WheelPicker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.to.dicegame.R;
import pl.to.dicegame.model.User;
import pl.to.dicegame.model.game.Game;
import pl.to.dicegame.rest.ConfigurationRoomController;

public class GameConfigurationActivity extends AppCompatActivity implements ConfigurationRoomController {

    @BindView(R.id.wheel_picker_players) WheelPicker playersWheelPicker;
    @BindView(R.id.wheel_picker_bots_easy) WheelPicker botsEasyWheelPicker;
    @BindView(R.id.wheel_picker_bots_hard) WheelPicker botsHardWheelPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        ButterKnife.bind(this);

        List<Integer> integers = new ArrayList<>();
        for(int i =0; i<100; i++){
            integers.add(i);
        }

        playersWheelPicker.setData(integers);
        botsEasyWheelPicker.setData(integers);
        botsHardWheelPicker.setData(integers);
    }

    @OnClick(R.id.start_game_button)
    void onNewGameStart(){

    }

    @Override
    public void onNewGameReceived(Game game) {

    }

    @Override
    public void onPlayerJoin(User user) {

    }
}
