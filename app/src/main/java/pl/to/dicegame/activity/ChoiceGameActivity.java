package pl.to.dicegame.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.to.dicegame.R;
import pl.to.dicegame.adapter.GameListAdapter;
import pl.to.dicegame.model.Configuration;
import pl.to.dicegame.model.User;
import pl.to.dicegame.model.game.AbstractGame;
import pl.to.dicegame.model.game.Game;
import pl.to.dicegame.model.game.NPlusGame;
import pl.to.dicegame.model.game.PokerGame;


public class ChoiceGameActivity extends AppCompatActivity {

    @BindView(R.id.game_list) ListView gameListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_choice);
        ButterKnife.bind(this);

        GameListAdapter gameListAdapter = new GameListAdapter(this, genereateTestGames());
        gameListView.setAdapter(gameListAdapter);
    }

    private List<AbstractGame> genereateTestGames(){
        List<User> users = new ArrayList<>();
        users.add(new User("Janek"));
        users.add(new User("Grzesiek"));
        users.add(new User("Krzysiek"));
        users.add(new User("Rysiek"));
        users.add(new User("Mirek"));
        users.add(new User("Jacek"));

        List<AbstractGame> games = new ArrayList<>();
        games.add(new PokerGame(users, true));
        games.add(new PokerGame(users, false));
        games.add(new PokerGame(users, true));
        games.add(new PokerGame(users, false));

        return games;
    }

    @OnClick(R.id.button_new_game)
    void onNewGameStart(){
        Intent mainIntent = new Intent(ChoiceGameActivity.this, GameConfigurationActivity.class);
        ChoiceGameActivity.this.startActivity(mainIntent);
        ChoiceGameActivity.this.finish();
    }
}
