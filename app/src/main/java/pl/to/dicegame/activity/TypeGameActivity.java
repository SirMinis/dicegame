package pl.to.dicegame.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.to.dicegame.R;

public class TypeGameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_type);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_n_plus)
    void onNPlusGame(){
        Intent mainIntent = new Intent(TypeGameActivity.this, ChoiceGameActivity.class);
        TypeGameActivity.this.startActivity(mainIntent);
        TypeGameActivity.this.finish();
    }

    @OnClick(R.id.button_n_star)
    void onNStarGame(){

    }

    @OnClick(R.id.button_poker)
    void onPokerGame(){

    }
}
