package pl.to.dicegame.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import pl.to.dicegame.R;
import pl.to.dicegame.jms.receiver.GameplayListener;
import pl.to.dicegame.model.game.Game;

public class MainActivity extends AppCompatActivity implements GameplayListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onTimeIsUp(Game game) {

    }

    @Override
    public void onActiveUserNick(String nick) {

    }

    @Override
    public void onNewStateOfGame(Game game) {

    }
}
