package pl.to.dicegame.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.to.dicegame.R;
import pl.to.dicegame.jms.NetworkController;
import pl.to.dicegame.rest.LoginCallback;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_text)
    EditText editLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.login_button)
    void onLogin() {
        new NetworkController().checkLogin(editLogin.getText().toString(), new LoginCallback() {
            @Override
            public void onSuccess() {
                Intent mainIntent = new Intent(LoginActivity.this, TypeGameActivity.class);
                LoginActivity.this.startActivity(mainIntent);
                LoginActivity.this.finish();
            }

            @Override
            public void onFailure() {

            }
        });
    }
}
