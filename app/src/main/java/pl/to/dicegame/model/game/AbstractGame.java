package pl.to.dicegame.model.game;

import java.util.List;

import pl.to.dicegame.model.User;


public abstract class AbstractGame implements Game {

    private List<User> user;
    private boolean started;

    public AbstractGame(List<User> user, boolean started) {
        this.user = user;
        this.started = started;
    }

    public List<User> getUser() {
        return user;
    }

    public boolean isStarted() {
        return started;
    }
}
