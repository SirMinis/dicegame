package pl.to.dicegame.model.game;

/**
 * Created by hector on 13.12.16.
 */

public interface Game {

    GameType getGameType();
}
