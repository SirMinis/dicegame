package pl.to.dicegame.model;

import pl.to.dicegame.model.game.GameType;


public class Configuration {

    private int humansNum;
    private int easyBotsNum;
    private int hardBotsNum;
    private GameType gameType;
    private int numberOfPointsToWin;

    public int getHumansNum() {
        return humansNum;
    }

    public void setHumansNum(int humansNum) {
        this.humansNum = humansNum;
    }

    public int getEasyBotsNum() {
        return easyBotsNum;
    }

    public void setEasyBotsNum(int easyBotsNum) {
        this.easyBotsNum = easyBotsNum;
    }

    public int getHardBotsNum() {
        return hardBotsNum;
    }

    public void setHardBotsNum(int hardBotsNum) {
        this.hardBotsNum = hardBotsNum;
    }

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }

    public int getNumberOfPointsToWin() {
        return numberOfPointsToWin;
    }

    public void setNumberOfPointsToWin(int numberOfPointsToWin) {
        this.numberOfPointsToWin = numberOfPointsToWin;
    }
}
