package pl.to.dicegame.model.move;

/**
 * Created by luke on 06.12.16.
 */
public enum MoveType {
    PASS, THROW
}
