package pl.to.dicegame.model.game;

/**
 * Created by hector on 13.12.16.
 */
public enum GameType {
    POKER, N_PLUS, N_STAR
}
