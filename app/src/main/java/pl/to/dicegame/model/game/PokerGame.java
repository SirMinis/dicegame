package pl.to.dicegame.model.game;

import java.util.List;

import pl.to.dicegame.model.User;

/**
 * Created by hector on 13.12.16.
 */
public class PokerGame extends AbstractGame {


    public PokerGame(List<User> user, boolean started) {
        super(user, started);
    }

    @Override
    public GameType getGameType() {
        return GameType.POKER;
    }
}
