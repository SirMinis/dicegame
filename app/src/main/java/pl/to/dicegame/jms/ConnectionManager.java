package pl.to.dicegame.jms;

import com.kaazing.gateway.jms.client.JmsConnectionFactory;
import com.kaazing.net.ws.WebSocketFactory;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;

import pl.to.dicegame.jms.receiver.DestinationMessageReceiver;

/**
 * Created by hector on 13.12.16.
 */

public class ConnectionManager {

    private final Map<String, List<MessageConsumer>> consumers = new HashMap<>();
    private JmsConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private String destinationName;
    private DispatchQueue dispatchQueue;
    private final DestinationMessageReceiver destinationMessageReceiver;

    public ConnectionManager(String destinationName, DestinationMessageReceiver destinationMessageReceiver) throws JMSException {
        this.destinationName = destinationName;
        this.destinationMessageReceiver = destinationMessageReceiver;
        connectionFactory = JmsConnectionFactory.createConnectionFactory();
        WebSocketFactory webSocketFactory = connectionFactory.getWebSocketFactory();
        connectionFactory.setGatewayLocation(null/* TODO URI */);
        connection = connectionFactory.createConnection();
    }

    private Destination getDestination() throws JMSException {
        Destination destination;
        // Create the topic using the destination name entered by the user
        if (destinationName.startsWith("/topic/")) {
            destination = session.createTopic(destinationName);
        }
        // Create the queue using the destination name entered by the user
        else if (destinationName.startsWith("/queue/")) {
            destination = session.createQueue(destinationName);
        } else {
            // TODO // ("Invalid destination name: " + destinationName);
            return null;
        }
        return destination;
    }

    public void establishConnection(String location) {
        dispatchQueue = new DispatchQueue("DispatchQueue");
        dispatchQueue.start();
        dispatchQueue.waitUntilReady();

        // Call the connect() method defined later in this code
        connect(location);
    }

    public void startListener() {
        dispatchQueue.dispatchAsync(new Runnable() {
            public void run() {
                try {
                    Destination destination = getDestination();
                    if (destination == null) {
                        return;
                    }

                    // Create a consumer using the destination
                    MessageConsumer consumer = session.createConsumer(destination);

                    // Create an array deque mapping consumers to the destination
                    List<MessageConsumer> consumersToDestination =
                            consumers.get(destinationName);
                    // If there is no consumers to destination map, add this new map
                    if (consumersToDestination == null) {
                        consumersToDestination = new ArrayList<MessageConsumer>();
                        consumers.put(destinationName, consumersToDestination);
                    }
                    // If there is a map, update it with the new consumer
                    consumersToDestination.add(consumer);
                    // Set a message listener in the DestinationMessageReceiver() method
                    consumer.setMessageListener(destinationMessageReceiver);
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void connect(final String location) {
        // ("CONNECTING");

        // Since createConnection() is a blocking method that does not return until 
        // the connection is established or fails, it is a good practice to 
        // establish the connection on a separate thread and prevent the TUI from being blocked.
        dispatchQueue.dispatchAsync(new Runnable() {
            public void run() {
                try {
                    // Set the target Gateway location
                    connectionFactory.setGatewayLocation(URI.create(location));
                    // Create the connection to the Gateway
                    connection = connectionFactory.createConnection();
                    // Connect to the Gateway
                    connection.start();

                    // Create a Session object and set the client to 
                    // acknowledge any messages it receives automatically
                    session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                    // ("CONNECTED");
                    // Set an exception listener for the client 
                    connection.setExceptionListener(new ConnectionExceptionListener());
                    // Update the TUI 
                    // TODO do sth e.g. updateButtonsForConnected();
                } catch (Exception e) {
                    // TODO do sth e.g. updateButtonsForDisconnected();
                    e.printStackTrace();
                    // ("EXCEPTION: " + e.getMessage());
                }
            }
        });
    }

}
