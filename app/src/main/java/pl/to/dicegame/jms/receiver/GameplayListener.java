package pl.to.dicegame.jms.receiver;

import pl.to.dicegame.model.game.Game;

/**
 * Created by hector on 13.12.16.
 */

public interface GameplayListener {
    void onTimeIsUp(Game game);
    void onActiveUserNick(String nick);
    void onNewStateOfGame(Game game);
}
