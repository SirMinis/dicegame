package pl.to.dicegame.jms.receiver;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Created by hector on 13.12.16.
 */

public abstract class DestinationMessageReceiver implements MessageListener {

    public void onMessage(Message message) { //respond when a message is received

        final Map<String, Object> key2MessageMap = new HashMap<>();
        try {
            if (message instanceof TextMessage) { // log test messages in the UI console
                // TODO ("RECEIVED TextMessage: " + ((TextMessage)message).getText());
            } else if (message instanceof BytesMessage) { // manage binary using BytesMessage
                BytesMessage bytesMessage = (BytesMessage) message;

                long len = bytesMessage.getBodyLength(); // order the binary and display hex in console
                byte b[] = new byte[(int) len];
                bytesMessage.readBytes(b);

                // TODO ("RECEIVED BytesMessage: " + hexDump(b)); // see this function below
            } else if (message instanceof MapMessage) { // manage MapMessage pairs, text or binary
                MapMessage mapMessage = (MapMessage) message;
                Enumeration mapNames = mapMessage.getMapNames();
                while (mapNames.hasMoreElements()) {
                    String key = (String) mapNames.nextElement();
                    Object value = mapMessage.getObject(key);

                    if (value == null) {
                        // TODO (key + ": null");
                    } else if (value instanceof byte[]) {
                        byte[] arr = (byte[]) value;
                        StringBuilder s = new StringBuilder(); // see this function below
                        s.append("[");
                        for (int i = 0; i < arr.length; i++) {
                            if (i > 0) {
                                s.append(",");
                            }
                            s.append(arr[i]);
                        }
                        s.append("]");
                        // TODO (key + ": "+ s.toString() + " (Byte[])"); // display binary name and value pairs
                    } else {


                        // ACHTUNG BARDZO WAŻNE TUTAJ
                        key2MessageMap.put(key, value);


                        // TODO (key + ": " + value.toString() + " (" + value.getClass().getSimpleName() + ")"); // display text name and value pairs
                    }
                }
                // TODO ("RECEIVED MapMessage: ");
            } else {
                // TODO ("UNKNOWN MESSAGE TYPE: "+message.getClass().getSimpleName());
            }

        } catch (Exception ex) { // handle exceptions
            ex.printStackTrace();
            // TODO ("EXCEPTION: " + ex.getMessage());
        }


        // ACHTUNG BARDZO WAŻNE TUTAJ
        this.callControllerMethod((String) key2MessageMap.get("interface"), key2MessageMap.get("object"));

    }

    protected abstract void callControllerMethod(String interfaceName, Object receivedObject);

    private String hexDump(byte[] b) { // convert the binary to hexadecimal and return
        if (b.length == 0) {
            return "empty";
        }

        StringBuilder out = new StringBuilder(); // convert MapMessage binary to hexadecimal pairs and return
        for (int i = 0; i < b.length; i++) {
            out.append(Integer.toHexString(b[i])).append(' ');
        }
        return out.toString();
    }

}