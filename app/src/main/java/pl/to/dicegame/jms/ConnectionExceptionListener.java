package pl.to.dicegame.jms;

import com.kaazing.gateway.jms.client.ConnectionDisconnectedException;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;

/**
 * Created by hector on 13.12.16.
 */
public class ConnectionExceptionListener implements ExceptionListener {

    public void onException(final JMSException exception) {
        if (exception instanceof ConnectionDisconnectedException) {
            // TODO do sth, e.g. updateButtonsForDisconnected();
        }
    }
}
