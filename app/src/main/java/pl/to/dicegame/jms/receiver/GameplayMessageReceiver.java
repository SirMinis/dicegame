package pl.to.dicegame.jms.receiver;

import pl.to.dicegame.model.game.Game;

/**
 * Created by hector on 13.12.16.
 */
public class GameplayMessageReceiver extends DestinationMessageReceiver {

    private final GameplayListener gameplayListener;

    public GameplayMessageReceiver(GameplayListener gameplayListener) {
        this.gameplayListener = gameplayListener;
    }

    @Override
    protected void callControllerMethod(String interfaceName, Object receivedObject) {
        switch (interfaceName) {
            case "TIME_IS_UP":
                gameplayListener.onTimeIsUp((Game) receivedObject);
        }
    }
}
